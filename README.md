tapp-prerender-s3
=======================

Prerender plugin for S3 caching with Expire Date, to be used with the prerender node application from https://github.com/prerender/prerender.

How to use
----------

In your local prerender project run:

    $ npm install git+ssh://git@bitbucket.org/businessontapp/tapp-prerender-s3.git --save

Then in the server.js that initializes the prerender:
```js
var dateCallback = function(){ return moment.add(1, 'day').toDate(); };
server.use(require('tapp-prerender-s3')(dateCallback));
```